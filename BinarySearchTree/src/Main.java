import java.util.ArrayList;
import java.util.function.Consumer;

/**
 *
 * @author Toni-Tran
 */
public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Character> charTree = new BinarySearchTree<>();
        charTree.insert('F');
        charTree.insert('B');
        charTree.insert('G');
        charTree.insert('A');
        charTree.insert('D');
        charTree.insert('I');
        charTree.insert('C');
        charTree.insert('E');
        charTree.insert('H');
        System.out.println("Preorder:");
        charTree.printTree(BinarySearchTree.Order.PRE_ORDER);
        System.out.println("\nInorder:");
        charTree.printTree(BinarySearchTree.Order.IN_ORDER);
        System.out.println("\nPostorder:");
        charTree.printTree(BinarySearchTree.Order.POST_ORDER);
        System.out.println("\nBreadth order:");
        charTree.printTree(BinarySearchTree.Order.BREADTH_ORDER);

        charTree.remove('F');
        charTree.remove('G');
        charTree.remove('D');
        charTree.remove('C');
        charTree.remove('H');
        System.out.println("\nPost removal of F,G,D,C,H:");
        charTree.printTree(BinarySearchTree.Order.BREADTH_ORDER);
        charTree.insert('Q');
        charTree.insert('Z');
        charTree.insert('B');
        charTree.insert('B');
        charTree.insert('B');
        charTree.insert('B');
        System.out.println("\nPost insertion of Q,Z,and several Bs:");
        charTree.printTree(BinarySearchTree.Order.BREADTH_ORDER);
        charTree.printDebug();
    }

}















class BinarySearchTree<T extends Comparable<T>> {

    public static enum Order {

        PRE_ORDER, IN_ORDER, POST_ORDER, BREADTH_ORDER
    };

    private BSTNode root;
    private int size;



    public BinarySearchTree() {
        size = 0;
    }



    /**
     * Inserts a piece of data into the BST. Relies upon a recursive helper. It
     * attempts to "replace" the root node with a modified version of itself, if
     * any modifications occurred.
     *
     * @param data
     *            The data to insert.
     */
    public void insert(T data) {
        /* Check for attempted null insertion. */
        if (data == null) {
            return;
        }
        root = insertHelper(root, data);
    }



    /**
     * Private helper method for the insertion method. Returns a modified
     * version of the passed in node.
     *
     * @param node
     *            The node to examine and modify for return.
     * @param data
     *            The data to be inserted in the correct spot, when found.
     * @return a modified version of the root and its sub-trees.
     */
    private BSTNode insertHelper(BSTNode node, T data) {
        /*
         * We have found a null location where we are supposed to insert a new
         * node.
         */
        if (node == null) {
            BSTNode n = new BSTNode();
            n.data = data;
            size++;
            return n;
        }
        if (node.data.compareTo(data) == 0) {
            /*
             * If we have found a node which already contains that particular
             * piece of data, return an unmodified version of itself to the
             * parent stack, one level above the current recursive stack.
             */
            return node;
        } else if (data.compareTo(node.data) < 0) {
            /*
             * If the place to insert is somewhere in the left sub-tree, recurse
             * to the left.
             */
            node.leftChild = insertHelper(node.leftChild, data);
        } else {
            /* Else, the only place left is to recurse to the right. */
            node.rightChild = insertHelper(node.rightChild, data);
        }
        /* Return the now modified sub-tree. */
        return node;
    }



    /**
     * Removes a piece of data from the BST. Relies upon a recursive helper. It
     * attempts to "replace" the root node with a modified version of itself, if
     * any modifications occurred.
     *
     * @param data
     *            The data to remove.
     */
    public void remove(T data) {
        if (data == null) {
            return;
        }
        root = removeHelper(root, data);
    }



    /**
     * Private helper method for the remove method. Returns a modified version
     * of the passed in node.
     *
     * @param node
     *            The node to examine and modify for return.
     * @param data
     *            The data to be removed from the correct spot, when found.
     * @return a modified version of the root and its sub-trees.
     */
    private BSTNode removeHelper(BSTNode node, T data) {
        if (node.data == data) {
            /*
             * We have found the node we need to remove. Does it have any
             * children we need to take care of?
             */
            if (node.leftChild == null && node.rightChild == null) {
                /*
                 * The node we are about to delete has no children at all.
                 * Simply return a null to replace the subtree.
                 */
                size--;
                return null;
            } else if (node.leftChild != null && node.rightChild != null) {
                /*
                 * The node we are about to delete has 2 children. First,
                 * replace the current node's data with the minimum of the right
                 * subtree. Then, delete that minimum node we just found.
                 */
                node.data = findMin(node.rightChild);
                node.rightChild = removeHelper(node.rightChild, node.data);
            } else {
                /*
                 * The node we are about to delete has 1 child. Return that
                 * child to replace the subtree. Note that I am using a ternary
                 * operation. It follows the format of (Condition)? returnIfTrue
                 * : returnIfFalse.
                 */
                size--;
                return (node.leftChild != null) ? node.leftChild
                        : node.rightChild;
            }
        } else if (data.compareTo(node.data) < 0) {
            /* We need to search the left sub-tree to find the node to remove. */
            node.leftChild = removeHelper(node.leftChild, data);
        } else {
            /* We need to search the right sub-tree to find the node to remove. */
            node.rightChild = removeHelper(node.rightChild, data);
        }
        /* Return the now modified sub-tree. */
        return node;
    }



    /**
     * Returns true if the BST is empty.
     *
     * @return true if the BST is empty.
     */
    public boolean isEmpty() {
        return size == 0;
    }



    /**
     * Returns the number of elements currently stored in the BST.
     *
     * @return the number of elements currently stored in the BST.
     */
    public int size() {
        return size;
    }



    /**
     * Finds the node with the minimum value located within a particular subtree
     * and returns its data.
     *
     * @param node
     *            The node to search through for a min value.
     * @return the node with the minimum value located within a particular
     *         subtree.
     */
    public T findMin(BSTNode node) {
        if (node.leftChild == null) {
            /* The node itself is already the minimum. */
            return node.data;
        }
        /* Recurse down the left subtree, repeatedly until we find the min node. */
        return findMin(node.leftChild);
    }



    /**
     * Finds the node with the minimum value located within a particular subtree
     * and returns its data.
     *
     * @param node
     *            The node to search through for a max value.
     * @return the node with the maximum value located within a particular
     *         subtree.
     */
    public T findMax(BSTNode node) {
        if (node.rightChild == null) {
            /* The node itself is already the maximum. */
            return node.data;
        }
        /* Recurse down the left subtree, repeatedly until we find the min node. */
        return findMax(node.rightChild);
    }



    /**
     * Prints the tree in the order specified.
     *
     * @param o
     *            The order to print the tree in.
     */
    public void printTree(Order o) {
        /**
         * Passes in a consumer which will perform an action on each of the
         * nodes it passes by.
         */
        Consumer<T> nodePrinter = (Consumer<T>) (T t) -> {
            System.out.print(t + ", ");
        };
        traverse(o, nodePrinter);
    }



    /**
     * Traverses the tree in the provided order, and performs an action upon
     * each node. The action is determined by the supplied Consumer object.
     *
     * @param o
     *            The order to traverse the tree in.
     * @param consumer
     *            The consumer which will perform an action on each of the nodes
     *            data.
     */
    public void traverse(Order o, Consumer<T> consumer) {
        switch (o) {
        case PRE_ORDER:
            traversePreOrder(root, consumer);
            break;
        case IN_ORDER:
            traverseInOrder(root, consumer);
            break;
        case POST_ORDER:
            traversePostOrder(root, consumer);
            break;
        case BREADTH_ORDER:
            traverseBreadthOrder(consumer);
            break;
        }
    }



    /**
     * Traverse the tree, pre-order and performs the provided consumer's action
     * on each node.
     *
     * @param node
     *            The node currently examined.
     * @param consumer
     *            The consumer object which contains an action to be performed
     *            on the current node's data.
     */
    private void traversePreOrder(BSTNode node, Consumer<T> consumer) {
        if (node == null) {
            return;
        }
        consumer.accept(node.data);
        traversePreOrder(node.leftChild, consumer);
        traversePreOrder(node.rightChild, consumer);
    }



    /**
     * Traverse the tree, in-order and performs the provided consumer's action
     * on each node.
     *
     * @param node
     *            The node currently examined.
     * @param consumer
     *            The consumer object which contains an action to be performed
     *            on the current node's data.
     */
    private void traverseInOrder(BSTNode node, Consumer<T> consumer) {
        if (node == null) {
            return;
        }
        traverseInOrder(node.leftChild, consumer);
        consumer.accept(node.data);
        traverseInOrder(node.rightChild, consumer);
    }



    /**
     * Traverse the tree, post-order and performs the provided consumer's action
     * on each node.
     *
     * @param node
     *            The node currently examined.
     * @param consumer
     *            The consumer object which contains an action to be performed
     *            on the current node's data.
     */
    private void traversePostOrder(BSTNode node, Consumer<T> consumer) {
        if (node == null) {
            return;
        }
        traversePostOrder(node.leftChild, consumer);
        traversePostOrder(node.rightChild, consumer);
        consumer.accept(node.data);
    }



    /**
     * Traverse the tree, breadth-first, and performs the consumer's action on
     * each node.
     *
     * @param consumer
     *            The consumer which contains the action to be performed on each
     *            node this method traverses.
     */
    private void traverseBreadthOrder(Consumer<T> consumer) {
        /* Set up a queue for nodes. */
        ArrayList<BSTNode> queue = new ArrayList<>();
        /* Initiate with the root. */
        queue.add(root);
        int iterator = 0;
        BSTNode currentNode;
        while (iterator != queue.size()) {
            /* Set the current node to perform an action on. */
            currentNode = queue.get(iterator);
            consumer.accept(currentNode.data);
            /* If it has a left child, add it to the queue. */
            if (currentNode.leftChild != null) {
                queue.add(currentNode.leftChild);
            }
            /* If it has a right child, add it to the queue. */
            if (currentNode.rightChild != null) {
                queue.add(currentNode.rightChild);
            }
            /*
             * Increment the counter so we can continue down the queue until
             * there are no more nodes left.
             */
            iterator++;
        }
    }



    /**
     * Prints debug information about the tree.
     */
    public void printDebug() {
        System.out.println("Size: " + size + "\n==========");
        printDebugHelper(root);
    }



    /**
     * Helper method that prints the tree, as matter of fact, in pre-order.
     *
     * @param node
     *            The node to examine.
     */
    private void printDebugHelper(BSTNode node) {
        System.out.println("Data: " + node.data);
        System.out.println("Left: " + node.leftChild);
        System.out.println("Right: " + node.rightChild);
        System.out.println("----------");
        if (node.leftChild != null) {
            printDebugHelper(node.leftChild);
        }
        if (node.rightChild != null) {
            printDebugHelper(node.rightChild);
        }
    }

    class BSTNode {

        private T data;
        BSTNode leftChild, rightChild;



        public BSTNode() {
            data = null;
            leftChild = null;
            rightChild = null;
        }



        @Override
        public String toString() {
            return data.toString();
        }
    }
}
